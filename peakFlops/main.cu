/*
file=main; rm $file.exe; nvcc -O0 -arch=sm_30 $file.cu -o $file.exe -std=c++11; ./$file.exe | tee $file.log
nvcc --std=c++11 -O0 --ptx main.cu
! Don't call with cuda-memcheck as it influences the time measurements !
*/


#include <iostream>
#include <iomanip>              // setw, setfill
#include <cstdlib>              // malloc, free
#include <cstdint>              // uint64_t, uint16_t
#include <cassert>
#include <cfloat>               // FLT_MAX
#include <vector>
#include <algorithm>            // minmax
#include <cuda.h>
#include <cuda_runtime_api.h>   // cudaMalloc, cudaFree, cudaMemcpy
#include "../cuda-common.h"

inline __device__ uint32_t getLaneId( void )
{
    uint32_t id;
    asm("mov.u32 %0, %%laneid;" : "=r"(id));
    return id;
}

/* needs __CUDA_ARCH__ >= 200 */
inline __device__ uint32_t bfe
(
    uint32_t const src,
    uint32_t const offset,
    uint32_t const nBits
)
{
    uint32_t result;
    asm( "bfe.u32 %0, %1, %2, %3;" :
         "=r"(result) : "r"(src), "r"(offset), "r"(nBits) );
    return result;
}

#include "a.cu"
#include "b.cu"
#include "c.cu"
#include "d.cu"
#include "e.cu"
#include "f.cu"
#include "g.cu"

    template<class T_CONTAINER>
    inline auto min( T_CONTAINER vec ) -> decltype( *vec.begin() )
    {
        return *std::min_element( vec.begin(), vec.end() );
    }
    template<class T_CONTAINER>
    inline auto max( T_CONTAINER vec ) -> decltype( *vec.begin() )
    {
        return *std::max_element( vec.begin(), vec.end() );
    }

    template<class T_PREC>
    T_PREC mean( std::vector<T_PREC> const vec )
    {
        auto sum = T_PREC(0);
        for ( auto const & elem : vec )
            sum += elem;
        return sum / vec.size();
    }
    template float mean<float>( std::vector<float> const vec );

    /**
     * < (x - <x>)^2 > = < x^2 + <x>^2 - 2x<x> > = <x^2> - <x>^2
     **/
    template<class T_PREC>
    T_PREC stddev( std::vector<T_PREC> const vec )
    {
        auto sum2 = T_PREC(0);
        for ( auto const elem : vec )
            sum2 += elem*elem;
        auto avg = mean( vec );
        auto const N = T_PREC( vec.size() );
        return sqrt( ( sum2/N - avg*avg )*N/(N-1) );
    }
    template float stddev( std::vector<float> const vec );

    template<class T_ELEM>
    std::ostream & operator<<( std::ostream &rOut, std::vector<T_ELEM> rVec )
    {
        rOut << "( ";
        for ( auto const & x : rVec )
            rOut << x << " ";
        rOut << ")";
        return rOut;
    }

void measureLatencyAndPipelining( void )
{

    unsigned int constexpr nRepetitions = 1024;
    uint64_t timing, * dpTiming;
    CudaMalloc( &dpTiming, 2 );

    uint64_t measurementOffset = UINT64_MAX;
    for ( auto i = 0u; i < nRepetitions; ++i )
    {
        kernelMeasurementOffset<<<1,1>>>( dpTiming );
        CudaMemcpyToHost( &timing, dpTiming );
        measurementOffset = std::min( measurementOffset, timing );
    }

    std::cout << "exponent | VecPower* | PragmaUnroll* | ManualUnroll* | 32 x exponent* | 32 x exponent(MAD)* | mul.Latency*�\n";
    std::cout << "*: GPU cycles | Host Duration / ms\n";
    std::cout << "�: In this case 'exponent' is only an abstract parameter, the real exponent is 2*floot( ld(exponent)/2 )\n";
    float * dpData;
    CudaMalloc( &dpData, 4096 );
    for ( int iPower = 1; iPower <= 2*4096; iPower *= 2 )
    {
        printf( "|% 6i |",iPower );

        cudaEvent_t start, stop;
        float time;
        cudaEventCreate( &start );
        cudaEventCreate( &stop );
        float minTimingHost;

        #define MEASURE_VECTORPOWER( KERNEL )                           \
        /* repeat measurement if the clock variable overflows ! */      \
        {                                                               \
            uint64_t minTiming  = UINT64_MAX;                           \
            minTimingHost = FLT_MAX;                                    \
            for ( auto i = 0u; i < nRepetitions; ++i )                  \
            {                                                           \
                CUDA_ERROR( cudaEventRecord( start, 0 ) );              \
                KERNEL<<<1,1>>>( dpData, iPower, dpTiming );            \
                CUDA_ERROR( cudaEventRecord( stop, 0 ) );               \
                CUDA_ERROR( cudaEventSynchronize( stop ) );             \
                cudaEventElapsedTime(&time, start, stop);               \
                minTimingHost = std::min( minTimingHost, time );        \
                CudaMemcpyToHost( &timing, dpTiming );                  \
                minTiming = std::min( minTiming, timing );              \
            }                                                           \
            printf( "%10lu", minTiming - measurementOffset );           \
            printf( "%7.3f", minTimingHost );                           \
            fflush( stdout );                                           \
        }

        MEASURE_VECTORPOWER( kernelVectorPower )
        MEASURE_VECTORPOWER( kernelVectorPowerPragmaUnroll )
        MEASURE_VECTORPOWER( kernelVectorPowerManualUnroll )
        MEASURE_VECTORPOWER( kernelVector32Power )
        MEASURE_VECTORPOWER( kernelVector32MAD )
        MEASURE_VECTORPOWER( kernelMulLatencyNoLoop )
        printf( "\n" );
    }
    CUDA_ERROR( cudaFree( dpTiming ) );
    CUDA_ERROR( cudaFree( dpData ) );

}

void getConcurrentThreadsPerSM( void )
{
    unsigned int constexpr nRepetitions = 128;
    unsigned int constexpr nMaxElements = 16*2048;
    unsigned int constexpr nPower       = 2048;
    unsigned int constexpr warpSize     = 32;
    unsigned int constexpr nMaxWarps    = 1024;
    unsigned int constexpr nSharedMP    = 6;
    uint64_t timing, * dpTiming;
    CudaMalloc( &dpTiming, 2 );

    uint64_t measurementOffset = UINT64_MAX;
    for ( auto i = 0u; i < nRepetitions; ++i )
    {
        kernelMeasurementOffset<<<1,1>>>( dpTiming );
        CudaMemcpyToHost( &timing, dpTiming );
        measurementOffset = std::min( measurementOffset, timing );
    }

    float * dpData;
    CudaMalloc( &dpData, nMaxElements );
    std::cout << "| nElements | nBlocks | nThreadsPerBlock | min cycles GPU | min time host / ms < +- stddev time host < max time host / ms | peakFlops / GFlops\n";
    for ( auto nThreadsPerBlock = warpSize; nThreadsPerBlock <= nMaxWarps;
          nThreadsPerBlock += warpSize )
    for ( auto nBlocks = nSharedMP; nBlocks * nThreadsPerBlock <= nMaxElements;
          nBlocks += nSharedMP )
    {
        printf( "|% 6i |", nBlocks * nThreadsPerBlock );
        printf( "% 4i |", nBlocks );
        printf( "% 5i |", nThreadsPerBlock );

        cudaEvent_t start, stop;
        cudaEventCreate( &start );
        cudaEventCreate( &stop );
        float time;

        std::vector<float> timingsHost;
        std::vector<float> peakGFlops;
        uint64_t minTiming  = UINT64_MAX;
        for ( auto i = 0u; i < nRepetitions; ++i )
        {
            CUDA_ERROR( cudaEventRecord( start, 0 ) );
            /* Note: too many threads while using to few blocks won't ever
             * get peak performance, because intra-block threads seemingly
             * have problems being pipelined ??? */
            kernelVector32MAD<<< nBlocks, nThreadsPerBlock >>>( dpData, nPower, dpTiming );
            CUDA_ERROR( cudaEventRecord( stop, 0 ) );
            CUDA_ERROR( cudaEventSynchronize( stop ) );

            cudaEventElapsedTime(&time, start, stop);
            timingsHost.push_back( time );
            peakGFlops.push_back( 2.0f*nBlocks*nThreadsPerBlock*nPower / (time*1e-3) / 1e9 );

            CudaMemcpyToHost( &timing, dpTiming );
            minTiming = std::min( minTiming, timing );
        }
        printf( "%8lu |", minTiming - measurementOffset );
        printf( "%6.3f < +- %6.3f < %6.3f", min( timingsHost ), stddev( timingsHost ), max( timingsHost ) );
        printf( " %8.3f",  max( peakGFlops ) );
        fflush( stdout );

        printf( "\n" );
    }
    CUDA_ERROR( cudaFree( dpTiming ) );
    CUDA_ERROR( cudaFree( dpData ) );

}

int main( void )
{
    getConcurrentThreadsPerSM();
    /* Furthermore interesting would be the execution time of
     * kernelVector32Power in dependence of the thread number (1 block constant)
     * Results for nPower = 4096*32:
     *  16.440s for 1,2,4,8,16,32,64,128,256,512,544,640,768 threads
     *  16.654s for 800 threads
     *  17.248s for 1024 threads -> all arithmetic unit on one SMX seem to be sated
     * It seems there are 768 compute unites per SMX ... oO
     * Time for 6 blocks with each 768 threads: 16.440s
     * Time for 6 blocks with each 800 threads: 16.884s
     * Time for 7 blocks with each 768 threads: 22.280s
     * => GTX 760 can do 768 * 6 fmul's in parallel with 1.15 GHz => 5.3 TFlops (SP-FP)
     * "Enabling Real-Time Mobile Cloud Computing through Emerging Technologies"
     *  -> The theoretical peak of GTX 760 is 2258 single precision GFLOP/s and
     *     86.8 double precision GFLOP/s.
     * http://hexus.net/tech/reviews/graphics/57029-nvidia-geforce-gtx-760/
     *  -> 1152 processors Oo? habe immer mit 384 gerechnet, weil 12288 / 32
     *  -> 12288 meaning the maximum concurrent threads has nothing to do with
     *     the number of CUDA cores availabl! Not even by a multiple of
     *     warpSize relation.
     *  -> this is also something I misunderstood: "Why is it possible, that
     *     I can't run as many threads as possible at some times?"
     *       -> Basically 1152 threads are enugh to fully utilize the GPU,
     *          but that may need some additional work to account for
     *          arithmetic instruction latency (pipelining)
     * http://www.geforce.com/hardware/desktop-gpus/geforce-gtx-760/specifications
     * Memory Speed: 6.0 Gbps
     * Memory Interface Width: 256-bit
     * Memory Bandwidth (GB/sec): 192.2 = 48 GFloat(SP)/s
     * GTX 760: 6 SMX * 192 CUDA Cores / SMX * 2 (MAD / FMA) * 0.98 GHz
     *      = 2257.92 GFLOP/s
     *  => I need at least 48 operations per float in order to not starve the
     *     bandwidth ... ??? TO TEST
     *
     * My CPU in comparison (i3-3220 @ 3.3GHz IvyBridge):
     *    FPUs: ( 1 AVX FPU ADD + 1 AVX FPU MUL ) / Core
     *    AVX width: 256 bit = 4 double
     *    Cores: 2 (4 logical)
     *  => 3.30GHz * 2 Core * 2 FPU / Core * 8 Single Precision Float / FPU
     *     = 105.6e9 FP-SP / s
     *     = 105.6 GFlops
     *
     * => expected speedup GPU / CPU:  21.3x
     *
     * http://images.nvidia.com/content/pdf/kepler/Tesla-K80-BoardSpec-07317-001-v05.pdf
     * Memory bandwidth: 480 GB/sec (cumulative)
     * Tesla K80: 13 SMX * 192 CUDA Cores / SMX 2 (MAD) * 0.875 GHz =
     *
     * How to calculate Peak Performance:
     * GTX 580: [1.544 GHz] * [512 CUDA Cores] *
                [2 double precision floating point operations/8 clock cycles]
                = 198 GFLOPS.
     * http://www.eetimes.com/author.asp?section_id=36&doc_id=1324326
     * "One the most powerful GPUs is the Nvidia Tesla K20. This GPU is based
     *  upon CUDA cores, each with a single floating-point multiple-adder unit
     *  that can execute one per clock cycle in single-precision floating-point
     *  configuration. There are 192 CUDA cores in each Streaming Multiprocessor
     *  (SMX) processing engine. The K20 actually contains 15 SMX engines,
     *  although only 13 are available (due to process yield issues, for
     *  example). This gives a total of 2,496 available CUDA cores, with two
     *  FLOPs per clock cycle, running at a maximum of 706 MHz. This provides
     *  a peak single-precision floating-point performance of 3,520 GFLOPs."
     *
     * http://www.computerbase.de/2013-06/nvidia-geforce-gtx-760-test/
     *   Chip: GK104
     *   Chiptakt: 980 MHz
     *   Boost-Takt: 1.033 MHz
     *   Shader-Einheiten: 1152
     *   FLOPs(MAD): 2.258 GFLOPs (MAD = Multiply Add ???)
     * "Ein interessantes Details am Rande: Nvidia liefert die GeForce GTX 760
     *  mit drei oder mit vier aktiven GPCs aus, je nach dem wo ein Defekt
     *  auf der GPU aufgetreten ist. �hnlich war Nvidia bei der GeForce GTX 780
     *  vorgegangen, bei der der Kunde vier oder f�nf GPCs zur Verf�gung hat."
     * Anmerkung: GPCs sind jeweils 4 Streaming Multiprocessors gruppiert, es
     *            k�nnen aber auch weniger sein. Die GTX 760 hat zwar variable
     *            GPC-Anzahl, aber immer 6 Streaming Multiprocessors gesamt.
     *            Das impliziert, dass bei den Versionen mit 4 GPCs ganze
     *            10/16 SMs defekt sind ... Das ist erschreckend.
     *
     * https://devblogs.nvidia.com/parallelforall/maxwell-most-advanced-cuda-gpu-ever-made/
     * SMM contains 4 32-core processing blocks
     * https://devblogs.nvidia.com/parallelforall/5-things-you-should-know-about-new-maxwell-gpu-architecture/
     *  "As with SMX, each SMM has four warp schedulers, but unlike SMX, all core SMM functional units are assigned to a particular scheduler, with no shared units. The power-of-two number of CUDA Cores per partition simplifies scheduling, as each of SMM's warp schedulers issue to a dedicated set of CUDA Cores equal to the warp width. Each warp scheduler still has the flexibility to dual-issue (such as issuing a math operation to a CUDA Core in the same cycle as a memory
operation to a load/store unit), but single-issue is now sufficient to fully utilize all CUDA Cores."
     *
     * NVVP: Based on the theoretical occupancy, device 'GeForce GTX 760' can
     * simultaneously execute 3 blocks on each of the 6 SMs, so the kernel may
     * need to execute a multiple of 18 blocks to hide the compute and memory
     * latency.
     *
     * <<<18,192>>> 16.440ms
     * <<<36,192>>> 17.905ms
     * <<<54,192>>> 25.953ms  // starting here proportional scaling -> sated
     * <<<78,192>>> 43.707ms
     *
     * http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#architecture-2-x
     * "Multiprocessors are grouped into Graphics Processor Clusters (GPCs). A GPC includes four multiprocessors. "
     */

    return 0;
}
