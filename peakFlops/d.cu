__global__ void kernelVector32Power
(
    float    * const  __restrict__ rpData,
    uint32_t   const rnPower,
    uint64_t * const __restrict__ rdpTiming
)
{
    auto x = rpData[ threadIdx.x ];
    auto y = x;
    uint64_t const t0 = clock64();
    for ( auto i = 0u; i < rnPower; ++i )
    {
        for ( auto j = 0u; j < 32; ++j )
            x *= y;
    }
    uint64_t const t1 = clock64();
    rpData[ threadIdx.x ] = x;

    /**
     * Translated to:
        mov.u64     %rd5, %clock64;
        // inline asm
        setp.eq.s32 %p1, %r4, 0;
        @%p1 bra    BB4_3;

        ldu.global.u64  %rd10, [%rd1];
        cvt.rn.f32.u64  %f2, %rd10;
        mov.u32     %r6, 0;

    BB4_2:
        mul.f32     %f6, %f37, %f2;
        mul.f32     %f7, %f6, %f2;
        mul.f32     %f8, %f7, %f2;
        mul.f32     %f9, %f8, %f2;
        mul.f32     %f10, %f9, %f2;
        mul.f32     %f11, %f10, %f2;
        mul.f32     %f12, %f11, %f2;
        mul.f32     %f13, %f12, %f2;
        mul.f32     %f14, %f13, %f2;
        mul.f32     %f15, %f14, %f2;
        mul.f32     %f16, %f15, %f2;
        mul.f32     %f17, %f16, %f2;
        mul.f32     %f18, %f17, %f2;
        mul.f32     %f19, %f18, %f2;
        mul.f32     %f20, %f19, %f2;
        mul.f32     %f21, %f20, %f2;
        mul.f32     %f22, %f21, %f2;
        mul.f32     %f23, %f22, %f2;
        mul.f32     %f24, %f23, %f2;
        mul.f32     %f25, %f24, %f2;
        mul.f32     %f26, %f25, %f2;
        mul.f32     %f27, %f26, %f2;
        mul.f32     %f28, %f27, %f2;
        mul.f32     %f29, %f28, %f2;
        mul.f32     %f30, %f29, %f2;
        mul.f32     %f31, %f30, %f2;
        mul.f32     %f32, %f31, %f2;
        mul.f32     %f33, %f32, %f2;
        mul.f32     %f34, %f33, %f2;
        mul.f32     %f35, %f34, %f2;
        mul.f32     %f36, %f35, %f2;
        mul.f32     %f37, %f36, %f2;
        add.s32     %r6, %r6, 1;
        setp.lt.u32 %p2, %r6, %r4;
        @%p2 bra    BB4_2;

    BB4_3:
        // inline asm
        mov.u64     %rd11, %clock64;
     **/

    /* compute benchmark:
        - FP32          : 90%
    */
    /* E.g. for rnPower = 64 and 32 threads (1 warp):
     * 19953 clocks needed, for rnPower = 10097
     * => 64*32 = fmul 2048 operations => 9.74 clocks per fmul
     *  2048 / ( 19953 / 1.15GHz ) = 11.80
     */

    if ( threadIdx.x == 0 )
        *rdpTiming = t1-t0;
}
