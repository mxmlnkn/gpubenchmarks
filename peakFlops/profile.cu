/*
file=profile; rm $file.exe; nvcc -O0 -arch=sm_30 $file.cu -o $file.exe -std=c++11; ./$file.exe
nvcc --std=c++11 -O0 --ptx profile.cu
! Don't call with cuda-memcheck as it influences the time measurements !
*/


#include <iostream>
#include <iomanip>              // setw, setfill
#include <cstdlib>              // malloc, free
#include <cstdint>              // uint64_t, uint16_t
#include <cassert>
#include <cfloat>               // FLT_MAX
#include <vector>
#include <algorithm>            // minmax
#include <cuda.h>
#include <cuda_runtime_api.h>   // cudaMalloc, cudaFree, cudaMemcpy
#include "../cuda-common.h"

#include "e.cu"

int main( void )
{
    unsigned int constexpr nRepetitions = 256;
    unsigned int constexpr nMaxElements = 16*2048;
    unsigned int constexpr nPower   = 256;
    unsigned int constexpr nBlocks  = 84;
    unsigned int constexpr nThreads = 384;

    uint64_t * dpTiming;
    CudaMalloc( &dpTiming, 2 );

    float * dpData;
    CudaMalloc( &dpData, nMaxElements );

    cudaEvent_t start, stop;
    cudaEventCreate( &start );
    cudaEventCreate( &stop );
    float time;

    float minTiming = UINT64_MAX;
    float maxFlops = 0;
    for ( auto i = 0u; i < nRepetitions; ++i )
    {
        CUDA_ERROR( cudaEventRecord( start, 0 ) );
        kernelVector32MAD<<< nBlocks, nThreads >>>( dpData, nPower, dpTiming );
        CUDA_ERROR( cudaEventRecord( stop, 0 ) );
        CUDA_ERROR( cudaEventSynchronize( stop ) );
        cudaEventElapsedTime( &time, start, stop );

        minTiming = std::min( minTiming, time );
        maxFlops  = std::max( maxFlops , 2.0f * nBlocks * nThreads * nPower*32 / (time*1e-3f) / 1e9f );
    }

    printf( "%6i ", nPower);
    printf( "%6.3f ", minTiming );
    printf( "%8.3f",  maxFlops );
    fflush( stdout );
    printf( "\n" );

    CUDA_ERROR( cudaFree( dpTiming ) );
    CUDA_ERROR( cudaFree( dpData ) );
}

// @TODO: maxConcurrentBlocks! (96)
