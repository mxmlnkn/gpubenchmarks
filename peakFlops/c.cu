__global__ void kernelVectorPowerManualUnroll
(
    float    * const  __restrict__ rpData,
    uint32_t   const rnPower,
    uint64_t * const __restrict__ rdpTiming
)
{
    /*
     * @see http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#pragma-unroll
     * If no number is specified after #pragma unroll, the loop is completely
     * unrolled if its trip count is constant, otherwise it is not unrolled at
     *  all.
     */
    auto x = rpData[ threadIdx.x ];
    uint64_t const t0 = clock64();
        const uint32_t div8 = rnPower >> 3;
        for ( auto i = 0u; i < div8; ++i )
        {
            #pragma unroll
            for ( auto j = 0u; j < 8; ++j )
                x *= x;
        }
        const uint32_t mod8 = rnPower & (8-1) ;
        for ( auto i = 0u; i < mod8; ++i )
        {
                x *= x;
        }
    uint64_t const t1 = clock64();
    rpData[ threadIdx.x ] = x;
    /* translates to */
    /**
        mov.u64     %rd5, %clock64;
        // inline asm
        shr.u32     %r2, %r8, 3;
        setp.eq.s32 %p1, %r2, 0;
        @%p1 bra    BB3_3;

        ldu.global.u64  %rd10, [%rd1];
        cvt.rn.f32.u64  %f2, %rd10;
        mov.u32     %r11, 0;

    BB3_2:
        add.f32     %f10, %f17, %f2;
        add.f32     %f11, %f10, %f2;
        add.f32     %f12, %f11, %f2;
        add.f32     %f13, %f12, %f2;
        add.f32     %f14, %f13, %f2;
        add.f32     %f15, %f14, %f2;
        add.f32     %f16, %f15, %f2;
        add.f32     %f17, %f16, %f2;
        add.s32     %r11, %r11, 1;
        setp.lt.u32 %p2, %r11, %r2;
        @%p2 bra    BB3_2;

    BB3_3:
        and.b32     %r5, %r8, 7;
        setp.eq.s32 %p3, %r5, 0;
        @%p3 bra    BB3_6;

        ldu.global.u64  %rd11, [%rd1];
        cvt.rn.f32.u64  %f6, %rd11;
        mov.u32     %r12, 0;

    BB3_5:
        add.f32     %f17, %f17, %f6;
        add.s32     %r12, %r12, 1;
        setp.lt.u32 %p4, %r12, %r5;
        @%p4 bra    BB3_5;

    BB3_6:
        // inline asm
        mov.u64     %rd12, %clock64;
    **/

    /* compute benchmark:
        - FP32          : 20%
        - Integer       : 45%
        - Control-Flow  : 20%
        - Misch         : 10%
    */

    if ( threadIdx.x == 0 )
        *rdpTiming = t1-t0;
}
