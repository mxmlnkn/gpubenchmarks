__global__ void kernelVectorPowerPragmaUnroll
(
    float    * const  __restrict__ rpData,
    uint32_t   const rnPower,
    uint64_t * const __restrict__ rdpTiming
)
{
    /*
     * @see http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#pragma-unroll
     * If no number is specified after #pragma unroll, the loop is completely
     * unrolled if its trip count is constant, otherwise it is not unrolled at
     *  all.
     */
    auto x = rpData[ threadIdx.x ];
    uint64_t const t0 = clock64();
        #pragma unroll 8
        for ( auto i = 0u; i < rnPower; ++i )
            x *= x;
    uint64_t const t1 = clock64();
    rpData[ threadIdx.x ] = x;
    /* translates to */
    /**
        mov.u64     %rd5, %clock64;
        // inline asm
        setp.eq.s32 %p1, %r11, 0;
        @%p1 bra    BB2_7;

        ldu.global.u64  %rd10, [%rd1];
        cvt.rn.f32.u64  %f2, %rd10;
        shr.s32     %r13, %r11, 31;
        shr.u32     %r14, %r13, 29;
        add.s32     %r15, %r11, %r14;
        shr.s32     %r2, %r15, 3;
        and.b32     %r3, %r15, -8;
        mov.u32     %r12, 0;
        setp.lt.s32 %p2, %r3, 1;
        mov.u32     %r18, %r12;
        @%p2 bra    BB2_5;

        shl.b32     %r16, %r2, 3;
        neg.s32     %r17, %r16;

    BB2_3:
        .pragma "nounroll";
        add.f32     %f9, %f16, %f2;
        add.f32     %f10, %f9, %f2;
        add.f32     %f11, %f10, %f2;
        add.f32     %f12, %f11, %f2;
        add.f32     %f13, %f12, %f2;
        add.f32     %f14, %f13, %f2;
        add.f32     %f15, %f14, %f2;
        add.f32     %f16, %f15, %f2;
        add.s32     %r17, %r17, 8;
        setp.ne.s32 %p3, %r17, 0;
        @%p3 bra    BB2_3;

        setp.eq.s32 %p4, %r3, %r11;
        mov.u32     %r18, %r3;
        @%p4 bra    BB2_7;

    BB2_5:
        sub.s32     %r19, %r18, %r11;

    BB2_6:
        .pragma "nounroll";
        add.f32     %f16, %f16, %f2;
        add.s32     %r19, %r19, 1;
        setp.ne.s32 %p5, %r19, 0;
        @%p5 bra    BB2_6;

    BB2_7:
        // inline asm
        mov.u64     %rd11, %clock64;
    **/
    /* compute benchmkark:
        - FP32          : 20%
        - Integer       : 45%
        - Control-Flow  : 20%
        - Misch         : 10%
    */

    if ( threadIdx.x == 0 )
        *rdpTiming = t1-t0;
}
