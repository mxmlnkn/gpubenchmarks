__global__ void kernelVector32MAD
(
    float    * const  __restrict__ rpData,
    uint32_t   const rnPower,
    uint64_t * const __restrict__ rdpTiming
)
{
    auto x = rpData[ threadIdx.x ];
    auto y = x;
    uint64_t const t0 = clock64();
    for ( auto i = 0u; i < rnPower; ++i )
    {
        for ( auto j = 0u; j < 32; ++j )
            x = x*y+7;
    }
    uint64_t const t1 = clock64();
    rpData[ threadIdx.x ] = x;

    /**
     * Translated to:
            mov.u64 	%rd5, %clock64;
            // inline asm
            setp.eq.s32	%p1, %r4, 0;
            mov.u32 	%r6, 0;
            mov.f32 	%f38, %f1;
            @%p1 bra 	BB6_3;

            mov.f32 	%f39, %f1;

        BB6_2:
            fma.rn.f32 	%f5, %f1, %f39, 0f40E00000;
            fma.rn.f32 	%f6, %f1, %f5, 0f40E00000;
            fma.rn.f32 	%f7, %f1, %f6, 0f40E00000;
            fma.rn.f32 	%f8, %f1, %f7, 0f40E00000;
            fma.rn.f32 	%f9, %f1, %f8, 0f40E00000;
            fma.rn.f32 	%f10, %f1, %f9, 0f40E00000;
            fma.rn.f32 	%f11, %f1, %f10, 0f40E00000;
            fma.rn.f32 	%f12, %f1, %f11, 0f40E00000;
            fma.rn.f32 	%f13, %f1, %f12, 0f40E00000;
            fma.rn.f32 	%f14, %f1, %f13, 0f40E00000;
            fma.rn.f32 	%f15, %f1, %f14, 0f40E00000;
            fma.rn.f32 	%f16, %f1, %f15, 0f40E00000;
            fma.rn.f32 	%f17, %f1, %f16, 0f40E00000;
            fma.rn.f32 	%f18, %f1, %f17, 0f40E00000;
            fma.rn.f32 	%f19, %f1, %f18, 0f40E00000;
            fma.rn.f32 	%f20, %f1, %f19, 0f40E00000;
            fma.rn.f32 	%f21, %f1, %f20, 0f40E00000;
            fma.rn.f32 	%f22, %f1, %f21, 0f40E00000;
            fma.rn.f32 	%f23, %f1, %f22, 0f40E00000;
            fma.rn.f32 	%f24, %f1, %f23, 0f40E00000;
            fma.rn.f32 	%f25, %f1, %f24, 0f40E00000;
            fma.rn.f32 	%f26, %f1, %f25, 0f40E00000;
            fma.rn.f32 	%f27, %f1, %f26, 0f40E00000;
            fma.rn.f32 	%f28, %f1, %f27, 0f40E00000;
            fma.rn.f32 	%f29, %f1, %f28, 0f40E00000;
            fma.rn.f32 	%f30, %f1, %f29, 0f40E00000;
            fma.rn.f32 	%f31, %f1, %f30, 0f40E00000;
            fma.rn.f32 	%f32, %f1, %f31, 0f40E00000;
            fma.rn.f32 	%f33, %f1, %f32, 0f40E00000;
            fma.rn.f32 	%f34, %f1, %f33, 0f40E00000;
            fma.rn.f32 	%f35, %f1, %f34, 0f40E00000;
            fma.rn.f32 	%f39, %f1, %f35, 0f40E00000;
            add.s32 	%r6, %r6, 1;
            setp.lt.u32	%p2, %r6, %r4;
            mov.f32 	%f37, %f39;
            mov.f32 	%f38, %f37;
            @%p2 bra 	BB6_2;

        BB6_3:
            // inline asm
            mov.u64 	%rd9, %clock64;
     **/

    /* compute benchmark:
        - FP32          : 90%
    */
    /* E.g. for rnPower = 64 and 32 threads (1 warp):
     * 19953 clocks needed, for rnPower = 10097
     * => 64*32 = fmul 2048 operations => 9.74 clocks per fmul
     *  2048 / ( 19953 / 1.15GHz ) = 11.80
     */

    if ( threadIdx.x == 0 )
        *rdpTiming = t1-t0;
}
