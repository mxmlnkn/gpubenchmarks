__global__ void kernelVectorPower
(
    float    * const  __restrict__ rpData,
    uint32_t   const rnPower,
    uint64_t * const __restrict__ rdpTiming
)
{
    auto x = rpData[ threadIdx.x ];
    uint64_t const t0 = clock64();
        for ( auto i = 0u; i < rnPower; ++i )
            x *= x;
    uint64_t const t1 = clock64();
    rpData[ threadIdx.x ] = x;
    /* translates to */
    /**
        mov.u64     %rd5, %clock64;
        // inline asm
        setp.eq.s32 %p1, %r4, 0;
        @%p1 bra    BB0_3;

        ldu.global.u64  %rd10, [%rd1];
        cvt.rn.f32.u64  %f2, %rd10;
        mov.u32     %r6, 0;

    BB0_2:
        add.f32     %f6, %f6, %f2;      // juicy part
        add.s32     %r6, %r6, 1;        // ++i          <- seem to be counted as integer operations
        setp.lt.u32 %p2, %r6, %r4;      // i < rnPower  <- seem to be counted as integer operations
        @%p2 bra    BB0_2;              // loop

    BB0_3:
        // inline asm
        mov.u64     %rd11, %clock64;
    **/
    /* compute benchmkark:
        - FP32          : 20%
        - Integer       : 45%
        - Control-Flow  : 20%
        - Misch         : 10%
    */

    if ( threadIdx.x == 0 )
        *rdpTiming = t1-t0;
}

__global__ void kernelMeasurementOffset
(
    uint64_t * const __restrict__ rdpTiming
)
{
    uint64_t t0 = clock64();
    uint64_t t1 = clock64();
    *rdpTiming = t1 - t0;
}
