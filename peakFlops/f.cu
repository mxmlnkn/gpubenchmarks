__global__ void kernelMulLatencyNoLoop
(
    float    * const  __restrict__ rpData,
    uint32_t   const rnPower,
    uint64_t * const __restrict__ rdpTiming
)
{
    auto x = rpData[ threadIdx.x ];
    uint64_t t0, t1;
    switch ( rnPower )
    {
        case 1:
        case 2:
            t0 = clock64();
                x *= x;
            t1 = clock64();
            break;
        case 4:
        case 8:
            t0 = clock64();
                x *= x;
                x *= x;
            t1 = clock64();
            break;
        case 16:
        case 32:
            t0 = clock64();
                x *= x;
                x *= x;
                x *= x;
                x *= x;
            t1 = clock64();
            break;
        case 64:
        case 128:
            t0 = clock64();
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
            t1 = clock64();
            break;
        case 256:
        case 512:
        default:
            t0 = clock64();
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
                x *= x;
            t1 = clock64();
            break;
    }
    rpData[ threadIdx.x ] = x;
    /* translates to */
    /**
        BB7_4:
            // inline asm
            mov.u64 	%rd16, %clock64;
            // inline asm
            mov.u64 	%rd22, %rd16;
            mul.f32 	%f10, %f1, %f1;
            // inline asm
            mov.u64 	%rd17, %clock64;
            // inline asm
            mov.u64 	%rd23, %rd17;
            bra.uni 	BB7_6;

        BB7_1:
            setp.eq.s32	%p2, %r2, 4;
            @%p2 bra 	BB7_3;

            setp.ne.s32	%p3, %r2, 8;
            @%p3 bra 	BB7_5;

        BB7_3:
            // inline asm
            mov.u64 	%rd14, %clock64;
            // inline asm
            mov.u64 	%rd22, %rd14;
            mul.f32 	%f6, %f1, %f1;
            mul.f32 	%f10, %f6, %f6;
            // inline asm
            mov.u64 	%rd15, %clock64;
            // inline asm
            mov.u64 	%rd23, %rd15;
            bra.uni 	BB7_6;

        BB7_5:
            // inline asm
            mov.u64 	%rd18, %clock64;
            // inline asm
            mov.u64 	%rd22, %rd18;
            mul.f32 	%f7, %f1, %f1;
            mul.f32 	%f8, %f7, %f7;
            mul.f32 	%f9, %f8, %f8;
            mul.f32 	%f10, %f9, %f9;
            // inline asm
            mov.u64 	%rd19, %clock64;
            // inline asm
            mov.u64 	%rd23, %rd19;
    **/

    if ( threadIdx.x == 0 )
        *rdpTiming = t1-t0;
}
