#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import argparse
import os as os

import seaborn as sns


def axisIsLog( ax, axis ):
    if axis == 'x':
        return ax.get_xscale() == 'log'
    elif axis == 'y':
        return ax.get_yscale() == 'log'
    else:
        assert( False, "axis neither 'x' nor 'y'!" )

def axisMin( ax, axis ):
    xmin=float('+inf')
    isLog = axisIsLog( ax, axis )
    for line in ax.get_lines():
        if axis == 'x':
            x = line.get_xdata()
        else:
            x = line.get_ydata()
        if isLog:
            x = x[ x>0 ]
        xmin = min( xmin, min(x) )
    return xmin

def axisMax( ax, axis ):
    xmax=float('-inf')
    isLog = axisIsLog( ax, axis )
    for line in ax.get_lines():
        if axis == 'x':
            x = line.get_xdata()
        else:
            x = line.get_ydata()
        if isLog:
            x = x[ x>0 ]
        xmax = max( xmax, max(x) )
    return xmax

def autoRange( ax, axis, lb, rb = None ):
    if rb == None:
        rb = lb
    isLog = axisIsLog( ax, axis )

    xmin = axisMin( ax, axis )
    xmax = axisMax( ax, axis )

    from math import log,exp

    if isLog:
        dx   = log(xmax) - log(xmin)
        xmin /= exp( lb*( dx ) )
        xmax *= exp( rb*( dx ) )
    else:
        dx = xmax - xmin
        xmin -= lb*dx
        xmax += rb*dx

    if axis == 'x':
        ax.set_xlim( [xmin,xmax] )
    else:
        ax.set_ylim( [xmin,xmax] )

def autoRangeXY( ax, lb = 0.1, rb = None, bb = None, tb = None ):
    if rb == None:
        rb = lb
    if tb == None:
        tb = lb
    if bb == None:
        bb = lb

    autoRange( ax, 'x', lb, rb )
    autoRange( ax, 'y', bb, tb )

def findSequences( x ):
    """
    Returns indices where a new sequence starts and the length of the
    sequences in part two of the returned tuple
    """
    x = np.array( x )
    n = len( x )
    if n == 0:
        return ( None, None )
    else:
        y = np.array( x[:-1] != x[1:] )             # pairwise unequal (string safe)
        i = np.flatnonzero( np.append( [True], y ) )
        l = np.diff( np.append( i, n+1 ) )          # run lengths
        assert len(i) == len(l)
        return ( i, l )

def shiftYWithAxisRatio( ax, y, r ):
    """
    returns a new y data point which represents the old y0 shifted by
    a ratio of the visible ylim, i.e. r=1 will increase y0 such that the new
    point would lie one y-axis-height above y0 (without a rescale)
    """
    y = np.array( y )
    y0 = ax.get_ylim()[0]
    y1 = ax.get_ylim()[1]
    from numpy import exp, log
    # r=0 => return y0, r=1 => return y1
    return y * exp( r * log( y1/y0 ) )

def addScaling( ax, x, y, exponent, xLabelPos = 0.5, interval = None, distance = 0.02, logscaling = None, color = 'k' ):
    """
    If left is True, then the scaling and the label will be put left /
    above of the data points x,y
    """
    assert len(x) == len(y)

    # Find significant intervals in (x,y) which scale with x^exponent
    x = np.log10( x )
    y = np.log10( y )
    from scipy.stats import linregress
    nPointsPerSection = min( 5, len( x ) )
    slopes = []
    from mkPython.ceilDiv import ceilDiv
    for iSection in range( ceilDiv( len(x), nPointsPerSection ) ):
        xSub = x[ iSection * nPointsPerSection : (iSection+1) * nPointsPerSection ]
        ySub = y[ iSection * nPointsPerSection : (iSection+1) * nPointsPerSection ]
        slope = linregress( xSub, ySub )[0]
        slopes += [ slope ]
    slopes = np.array( slopes )
    #print "slopes =",slopes

    if ( interval is None ) or ( len( interval ) == 0 ) or ( interval[0] is None ) or ( ( len( interval ) > 1 ) and interval[1] is None ):
        # select all those 30% close to exponent
        iCloseSlopes = np.less_equal( np.abs( ( slopes - exponent ) / exponent ), 0.3 )
        #print "iCloseSlopes =",iCloseSlopes
        # fill in some gaps shorther than 1 per 5 sections
        # gabs does not the first and last sequences.
        # Also only fill gaps which are false i.e. not close enough to the
        # exponent
        iPos, lengths = findSequences( iCloseSlopes )
        for i in range( 1, len( lengths )-1 ):
            if lengths[i] < len( iCloseSlopes ) / 5:
                iCloseSlopes[ iPos[i] : iPos[i] + lengths[i] ] = True
        #print "iCloseSlopes (filled) =",iCloseSlopes

        # Find the longest sequence now
        iPos, lengths = findSequences( iCloseSlopes )
        iStart = iPos[ np.argmax( lengths ) ] * nPointsPerSection
        #print "iPos =",iPos,", lengths =",lengths
        #print "iStart =",iStart,", max(lengths) =", np.max(lengths)
        xScaling = x[ iStart : iStart + np.max( lengths ) * nPointsPerSection ]
        yScaling = y[ iStart : iStart + np.max( lengths ) * nPointsPerSection ]

        iFix = iStart + np.max( lengths ) * nPointsPerSection / 2
    else:
        if 0 <= interval[0] and interval[0] <= 1:
            xmin = np.min(x) + interval[0] * ( np.max(x) - np.min(x) )
        else:
            xmin = np.log10( interval[0] )
        if 0 <= interval[1] and interval[1] <= 1:
            xmax = np.min(x) + interval[1] * ( np.max(x) - np.min(x) )
        else:
            xmax = np.log10( interval[1] )
        iToUse = np.flatnonzero( np.logical_and( x >= xmin, x <= xmax ) )
        xScaling = x[ iToUse ]
        yScaling = y[ iToUse ]
        # @todo use a point in the middle to affix the y-shift instead off the
        #       the first or the last which can have a noticable different slope
        iFix = iToUse[ len( iToUse ) / 2 ]
    x0 = np.min( xScaling )  # is already the log of the source data!
    x1 = np.max( xScaling )
    y0 = np.min( yScaling )
    #print "x0 =",x0,", x1 =",x1,", y0 =",y0
    #print "iFix =",iFix

    def scaling(x):
        if logscaling is None:
            return x ** exponent
        elif logscaling == 'log':
            return np.log2( x )
        elif logscaling == 'nlog':
            return x * np.log2( x )
        elif logscaling == 'nlog2':
            return x * np.log2( x )**2
    xScaling = 10.**( np.linspace( x0, x1, 200 ) )
    yScaling = scaling( xScaling )
    yScaling *= 10**y[iFix] / scaling( 10**x[iFix] )

    # arrange the scaling data to look good, shift it a bit away from the data
    # in loglogplot to shift by d orthogonal this means we need to shift y by
    #  a > .''.d .'
    #    .'y|  :'  cos a = d / y
    #  .'___|.'
    #.'  ^ .'
    #    a = atan2( exp, 1 )
    #  => y = d / cos atan2( exp, 1 )
    #distance = 0.02
    yDistance = -distance / np.cos( np.arctan2( exponent, 1 ) )
    #print "yDistance =",yDistance,", exponent =",exponent
    yScaling = shiftYWithAxisRatio( ax, yScaling, yDistance )

    ax.plot( xScaling, yScaling, linestyle = '-', color = color )
    # add text label
    xLabel = xScaling[ int( xLabelPos * len( xScaling ) ) ]
    yLabel = yScaling[ int( xLabelPos * len( xScaling ) ) ]
    #distance = 0
    yDistance = 0# -distance / np.cos( np.arctan2( exponent, 1 ) )
    yLabel = shiftYWithAxisRatio( ax, yLabel, yDistance )

    if logscaling is None:
        formula = "N"
        if exponent != 1:
            formula += r"^" + str( exponent )
    elif logscaling == 'log':
        formula = r"\mathrm{log}\left( N \right)"
    elif logscaling == 'nlog':
        formula = r"N \mathrm{log}\left( N \right)"
    elif logscaling == 'nlog2':
        formula = r"N \mathrm{log}^2\left( N \right)"
    ax.annotate( r"$\mathcal{O}\left(" + formula + r"\right)$",
                 ( xLabel, yLabel ),
                 horizontalalignment = 'left' if distance > 0 else 'right',
                 verticalalignment = 'top' if distance > 0 else 'bottom',
                 color = color )



def finishPlot( fig, ax, fname, loc='best', left=None, right=None, top=None, bottom=None ):
    """
    Give ax = [] in order to not draw a legend
    """
    fname = fname.replace( ":", "" )
    if not isinstance( ax, list ):
        ax = [ ax ]
    for a in ax:
        l = a.legend( loc=loc, labelspacing=0.2,
                      frameon=True, fancybox=True, framealpha=0.8 )
        if l != None:
            l.get_frame().set_facecolor('#FFFFFF')
        #if l != None:
        #    l.set_zorder(0)  # alternative to transparency

    fig.tight_layout()
    fig.subplots_adjust( left=left, bottom=bottom, right=right, top=top )

    fig.savefig( fname+".pdf" )
    print "[Saved '"+fname+".pdf']"
    fig.savefig( fname+".png" )
    print "[Saved '"+fname+".png']"
    from matplotlib.pyplot import close
    #close( fig )

def smoothMonotonicIncreasingData( x, y ):
    """ x has not really a valu, it's just filtered in the same manner as y
        y values are assumed to be in order, i.e. y[1] is right of y[0].
        I.e. if we plot y(x) then x must be monotonically increasing """
    N = float('Inf')
    while not ( len(y) == N ):
        filter = y[:-1] < 1.1 * y[1:]
        filter = np.concatenate( ( filter, [ True ] ) )
        N = len(y)
        y = y[ filter ]
        x = x[ filter ]
    return x,y

def addLogicalByteAxis( ax ):
    ax.set_title( ax.get_title(), y = 1.15 )
    with sns.axes_style( "ticks" ): #, { 'axes.linewidth':0.85, 'xtick.major.size': 3, 'xtick.minor.size': 3, 'ytick.major.size': 3, 'ytick.minor.size': 3 } ), sns.plotting_context( "notebook", font_scale=.9 ):
        ax2 = ax.twiny()
        ax2.tick_params( direction='in', pad=2 ) # move tick labels closer to axis (standard seems to be 5)
        ax2.grid( b = False )
        ax2.set_xlabel( "Size of list to sort", labelpad=7.0 )
        elemSize = 4 # 4 for float, 8 Byte for double
        ax2.set_xlim( [ elemSize * ax.get_xlim()[0], elemSize * ax.get_xlim()[1] ] )
        ax2.set_xscale( ax.get_xscale(), basex=2 ) # basex will be ignored for linear scale!

        import matplotlib.ticker as mtick
        def ticks( y, pos ):
            suffixes = [ 'B', 'kiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB' ]
            for i in range( len(suffixes) ):
                if y <= 1024:
                    return "%.0f " % ( y ) + suffixes[i]
                y /= 1024
        ax2.xaxis.set_major_formatter( mtick.FuncFormatter(ticks) )



sns.set( font_scale = 1.2 )
sns.set_style( "whitegrid" )
#https://stanford.edu/~mwaskom/software/seaborn/tutorial/color_palettes.html
sns.set_palette( "Set2", n_colors = 8, desat=.9 )  # relatively bright, may be hard ot see on beamer

parser = argparse.ArgumentParser()
#parser.add_argument("rundir", help="Directory which contains 'simOutput'")
parser.add_argument( "datafile",
    help    = "File with data from benchmarks",
    type    = str,
    nargs   = '?',
    default = "sorting-benchmarks.dat"
)
args = parser.parse_args()
rawData = np.genfromtxt( args.datafile )

figureSize = (8,5)
sns.set_context(rc={"figure.figsize": figureSize})
fig = plt.figure( figsize = figureSize )
ax = fig.add_subplot( 111, title="Shared memory benchmark" )
strides = np.arange( 1, 1+rawData.shape[1]-1 )
for iThreads in [1,2,4,8,16,32]:
    ax.plot( strides, rawData[ iThreads-1, 1: ], marker = 'o', label = "%i threads" % rawData[iThreads-1,0] )

finishPlot( fig, ax, "shared-float" )
plt.show()
