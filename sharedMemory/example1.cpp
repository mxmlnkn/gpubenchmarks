/*
file=example1; rm -f "$file.exe"; nvcc -x cu -Xcompiler -Wall,-Wextra -O0 -arch=sm_30 -I .. "../sharedMemory/$file.cpp" -o "$file.exe" -std=c++11 && "./$file.exe"
*/

#include <cassert>
#include <cstdint>              // uint64_t, uint16_t
#include <cstdlib>              // malloc, free
#include <iomanip>              // setw, setfill
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <cuda.h>
#include <cuda_runtime_api.h>   // cudaMalloc, cudaFree, cudaMemcpy

#include "cudacommon.hpp"


std::string printSharedMemoryConfig( void )
{
    std::stringstream out;
    out << "[Shared Memory] ";

    out << "Config: Prefer ";
    cudaFuncCache funcCache;
    cudaDeviceGetCacheConfig( &funcCache );
    switch ( funcCache )
    {
        case cudaFuncCachePreferNone  : out << "None"  ; break;
        case cudaFuncCachePreferShared: out << "Shared"; break;
        case cudaFuncCachePreferL1    : out << "L1"    ; break;
        case cudaFuncCachePreferEqual : out << "Equal" ; break;
        default: printf( "?" );
    }

    out << ", Bank Size: ";
    cudaSharedMemConfig config;
    cudaDeviceGetSharedMemConfig( &config );
    switch ( config )
    {
        case cudaSharedMemBankSizeDefault  : out << "Default"; break;
        case cudaSharedMemBankSizeFourByte : out << "4 Bytes"; break;
        case cudaSharedMemBankSizeEightByte: out << "8 Bytes"; break;
        default: printf( "?" );
    }
    out << "\n";
    return out.str();
}


__global__ void kernelClock64Latency
(
    long long unsigned int * const dt
)
{
    auto const t0 = clock64();
    auto const t1 = clock64();
    if ( threadIdx.x == 0 )
        *dt = t1-t0;
    /* does not depend on number of threads:
     * GTX 760: 105 */
}

__global__ void kernelClockLatency
(
    long long unsigned int * const dt
)
{
    auto const t0 = clock();
    auto const t1 = clock();
    if ( threadIdx.x == 0 )
        *dt = t1-t0;
    /* does not depend on number of threads:
     * GTX 760: 32 */
}

struct Color { float r,g,b,a; };

__global__ void kernelSharedArrayOfStruct
(
    long long unsigned int * const dt
)
{
    assert( blockDim.x == 32 );

    __shared__ __volatile__ Color smData[ 32*32 ];

    auto const t0 = clock();

    float sum = 1;
    for ( auto i = threadIdx.x; i < 32*32; i += blockDim.x )
        sum *= smData[i].r * smData[i].g * smData[i].b * smData[i].a;

    auto const t1 = clock();
    if ( threadIdx.x == 0 )
        *dt = t1-t0 + 1e-200 * sum;
    /* does not depend on number of threads:
     * GTX 760: 32 */
}

__global__ void kernelSharedStructOfArray
(
    long long unsigned int * const dt
)
{
    assert( blockDim.x == 32 );

    __shared__ __volatile__ float smDataR[ 32*32 ];
    __shared__ __volatile__ float smDataG[ 32*32 ];
    __shared__ __volatile__ float smDataB[ 32*32 ];
    __shared__ __volatile__ float smDataA[ 32*32 ];  // note: could also make conflict sizes weird?

    auto const t0 = clock();

    float sum = 1;
    for ( auto i = threadIdx.x; i < 32*32; i += blockDim.x )
        sum *= smDataR[i] * smDataG[i] * smDataB[i] * smDataA[i];

    auto const t1 = clock();
    if ( threadIdx.x == 0 )
        *dt = t1-t0 + 1e-200 * sum;
    /* does not depend on number of threads:
     * GTX 760: 32 */
}


int main( void )
{
    MirroredVector< long long unsigned int > time( 256 );
    kernelClockLatency  <<< 1, 32 >>>( time.gpu+0 );
    kernelClock64Latency<<< 1, 32 >>>( time.gpu+1 );
    kernelSharedArrayOfStruct<<< 1, 32 >>>( time.gpu+2 );
    kernelSharedStructOfArray<<< 1, 32 >>>( time.gpu+3 );
    time.pop();
    std::cout << "clock   latency: " << time.cpu[0] << std::endl;
    std::cout << "clock64 latency: " << time.cpu[1] << std::endl;
    std::cout << "AoS     latency: " << time.cpu[2] << std::endl;
    std::cout << "SoA     latency: " << time.cpu[3] << std::endl;
    /* only with __volatile__ is AoS slower ...
        clock   latency: 32
        clock64 latency: 105
        AoS     latency: 4489
        SoA     latency: 3134
    */
    return 0;
}
