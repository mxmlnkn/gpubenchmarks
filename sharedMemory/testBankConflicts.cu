/*
file=testBankConflicts; rm -f "$file.exe"; nvcc -Xcompiler -Wall,-Wextra -O0 -arch=sm_30 "../sharedMemory/$file.cu" -o "$file.exe" -std=c++11 && "./$file.exe"
! Don't call with cuda-memcheck as it influences the time measurements !
*/

/**
@verbatim
      +----+----+----+----+----+----+ ... +----+----+----+----+----+
Bank: |  0 |  1 |  2 |  3 |  4 |  5 |     | 27 | 28 | 29 | 30 | 31 |
      +----+----+----+----+----+----+ ... +----+----+----+----+----+
Words:|  0 |  1 |  2 |  3 |  4 |  5 |     | 27 | 28 | 29 | 30 | 31 |
      | 32 | 33 | 34 | 35 | 36 | 37 |     | 59 | 60 | 61 | 62 | 63 |
        ...
1 word = 4 byte = 32 bit, but one bank has a capacity of 64 bits per cycle
        ^ use that scheme to visualize accesses!
           float * smData
conflict : x = smData[ threadIdx.x * 32 ]
broadcast: x = smData[ 0 ]
perfect  : x = smData[ threadIdx.x ]

With 64K -> 64*1024/4/32 = 512 rows

@see https://youtu.be/CZgM3DEBplE
@endverbatim
*/

#include <cassert>
#include <cstdint>              // uint64_t, uint16_t
#include <cstdlib>              // malloc, free
#include <iomanip>              // setw, setfill
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <cuda.h>
#include <cuda_runtime_api.h>   // cudaMalloc, cudaFree, cudaMemcpy


constexpr int nMaxThreads = 48;
constexpr int nMaxStride  = 128;


/*
Facts:
    http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory-3-0
     - Shared memory has 32 banks two addressing modes that are described below. [32 vs. 64 bit per bank]
     - The addressing mode can be queried using cudaDeviceGetSharedMemConfig() and set using cudaDeviceSetSharedMemConfig()
     - Each bank has a bandwidth of 64 bits per clock cycle.
     http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1g318e21528985458de8613d87da832b42
        __host__  __device__ cudaError_t cudaDeviceGetSharedMemConfig ( cudaSharedMemConfig * pConfig )
        The returned bank configurations can be either:
          * cudaSharedMemBankSizeFourByte
          * cudaSharedMemBankSizeEightByte

     - https://devtalk.nvidia.com/default/topic/834795/shared-memory-bank-size-mode-4-byte-vs-8-byte-kepler-/?offset=8
       -> no difference between 8-Byte and 4-Byte bank size, if __syncthreads() is used ... ... WHY?
*/


void checkCudaError(const cudaError_t rValue, const char * file, int line )
{
    if ( (rValue) != cudaSuccess )
    std::cout << "CUDA error in " << file
              << " line:" << line << " : "
              << cudaGetErrorString(rValue) << "\n";
}
#define CUDA_ERROR(X) checkCudaError(X,__FILE__,__LINE__);

/**
 * atomicAdd for double is not natively implemented, because it's not
 * supported by (all) the hardware, therefore resulting in a time penalty.
 * http://stackoverflow.com/questions/12626096/why-has-atomicadd-not-been-implemented-for-doubles
 */
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                             (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
              __double_as_longlong(val + __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}


__device__ inline void atomicAdd
(
    uint16_t * const rdpTarget,
    const uint16_t rValue
)
{
    int assumed;
    int old = *rdpTarget;
    do
    {
        assumed = old;
        old = atomicCAS( (int*) rdpTarget, assumed,
            __float_as_int( assumed + rValue ) );
        old &= 0xFF;
    }
    while ( assumed != old );
}

__device__ inline void atomicAdd
(
    char * const rdpTarget,
    const char rValue
)
{
    int assumed;
    int old = *rdpTarget;
    do
    {
        assumed = old;
        old = atomicCAS( (int*) rdpTarget, assumed,
            __float_as_int( assumed + rValue ) );
        old &= 0x0F;
    }
    while ( assumed != old );
}

std::string printSharedMemoryConfig( void )
{
    std::stringstream out;
    out << "[Shared Memory] ";

    out << "Config: Prefer ";
    cudaFuncCache funcCache;
    cudaDeviceGetCacheConfig( &funcCache );
    switch ( funcCache )
    {
        case cudaFuncCachePreferNone  : out << "None"  ; break;
        case cudaFuncCachePreferShared: out << "Shared"; break;
        case cudaFuncCachePreferL1    : out << "L1"    ; break;
        case cudaFuncCachePreferEqual : out << "Equal" ; break;
        default: printf( "?" );
    }

    out << ", Bank Size: ";
    cudaSharedMemConfig config;
    cudaDeviceGetSharedMemConfig( &config );
    switch ( config )
    {
        case cudaSharedMemBankSizeDefault  : out << "Default"; break;
        case cudaSharedMemBankSizeFourByte : out << "4 Bytes"; break;
        case cudaSharedMemBankSizeEightByte: out << "8 Bytes"; break;
        default: printf( "?" );
    }
    out << "\n";
    return out.str();
}


template<class T_PREC>
__global__ void kernelTestStridedSharedMemory
(
    unsigned   const rnStride,
    T_PREC   * const __restrict__ rdpData,
    uint64_t * const __restrict__ rdpTiming
)
{
    assert( threadIdx.y == 0 );
    assert( threadIdx.z == 0 );
    assert( rnStride <= nMaxStride );

    //extern __shared__ __align__( sizeof(T_PREC) ) unsigned char dynamicSharedMemory[];
    //T_PREC * const smBlock = reinterpret_cast<T_PREC*>( dynamicSharedMemory );

    /* shared memory has 32 banks a 4 byte normally */
    constexpr unsigned nElements = nMaxStride*nMaxThreads;
    __shared__ T_PREC smData[ nElements ];
    /* we don't want to measure pointer arithmetic */
    T_PREC * const elem = &smData[ threadIdx.x * rnStride];
    for ( unsigned i = threadIdx.x; i < nElements; i += blockDim.x )
        smData[i] = T_PREC(1);
    __syncthreads();

    /* measure measurement offset, meaning the call to clock64 to subtract it */
    //__syncthreads();
    uint64_t t00 = clock64();
    //__syncthreads();
    uint64_t t10 = clock64();
    uint64_t dtOffset = t10 - t00;

    /* clock64 actually returns the GPU clocks, meaning we won't need a for
     * loop to wait and wait until the timer actually has enough precision to
     * measure that result. Here we can measure every cycle */
    //__syncthreads();
    uint64_t t0 = clock64();
    auto x = *elem;
    //__syncthreads(); // this seems to an effect on bank conflicts... ,see top
    uint64_t t1 = clock64();

    /* just some code to avoid optimizing out */
    const int32_t laneId = threadIdx.x % 32;
    #pragma unroll
    for ( int32_t warpDelta = 32 / 2; warpDelta > 0; warpDelta /= 2)
        x += __shfl_down( x, warpDelta );
    if ( laneId == 0 )
        atomicAdd( smData, x );
    __syncthreads();
    rdpData[0] = smData[0];

    if ( threadIdx.x == 0 )
    {
        if ( t1 > t0+dtOffset )
            *rdpTiming = t1-t0-dtOffset;
        else
            *rdpTiming = UINT64_MAX;
    }
}

template<class T_PREC>
__global__ void kernelTestStridedGlobalMemory
(
    unsigned   const rnStride,
    T_PREC   * const __restrict__ rdpData,
    uint64_t * const __restrict__ rdpTiming
)
{
    assert( threadIdx.y == 0 );
    assert( threadIdx.z == 0 );
    assert( rnStride <= nMaxStride );

    T_PREC * const elem = &rdpData[ threadIdx.x * rnStride ];

    //__syncthreads();
    uint64_t t00 = clock64();
    //__syncthreads();
    uint64_t t10 = clock64();
    uint64_t dtOffset = t10 - t00;

    //__syncthreads();
    uint64_t t0 = clock64();
    auto x = *elem;
    //__syncthreads(); // this seems to an effect on bank conflicts... ,see top
    uint64_t t1 = clock64();

    /* just some code to avoid optimizing out */
    const int32_t laneId = threadIdx.x % 32;
    #pragma unroll
    for ( int32_t warpDelta = 32 / 2; warpDelta > 0; warpDelta /= 2)
        x += __shfl_down( x, warpDelta );
    if ( laneId == 0 )
        atomicAdd( rdpData, x );

    if ( threadIdx.x == 0 )
    {
        if ( t1 > t0+dtOffset )
            *rdpTiming = t1-t0-dtOffset;
        else
            *rdpTiming = UINT64_MAX;
    }
}


template<class T_PREC>
void benchmarkStridedSharedMemoryAccess
( bool testGlobalMemory = false )
{
    void * dpReduced;
    CUDA_ERROR( cudaMalloc( &dpReduced, sizeof(T_PREC) * nMaxThreads * nMaxStride ) );
    uint64_t *dpTiming, timing;
    CUDA_ERROR( cudaMalloc( &dpTiming, sizeof( dpTiming[0] ) ) );

    std::ofstream fTxt( "BankConflicts.txt", std::ios::out );  // almost Ascii-art categorizable tables

    fTxt << "==================\n";
    switch ( sizeof(T_PREC) )
    {
        case 1: fTxt << "=== Using char ===\n"; break;
        case 2: fTxt << "= Using uint16_t =\n"; break;
        case 4: fTxt << "== Using float ===\n"; break;
        case 8: fTxt << "== Using double ==\n"; break;
        default:
            assert( false && "Please add used type in switch" );
    }
    fTxt << "==================\n";
    if ( not testGlobalMemory )
        fTxt << printSharedMemoryConfig();


    std::stringstream fDatName;
    fDatName << "BankConflicts-";
    switch ( sizeof(T_PREC) )
    {
        case 1: fDatName << "char"    ; break;
        case 2: fDatName << "uint16_t"; break;
        case 4: fDatName << "float"   ; break;
        case 8: fDatName << "double"  ; break;
        default: assert( false && "Please add used type in switch" );
    }
    fDatName << ".dat";
    std::ofstream fDat( fDatName.str(), std::ios::out );  // raw benchmarking data
    if ( not testGlobalMemory )
        fDat << "# " << printSharedMemoryConfig();

    const int colWidth = 6;
    /* +-------+-----+-----+-----+-----+ */
    fTxt << "+-------+";
    for ( int i = 0; i <= nMaxStride; ++i )
        fTxt << std::setfill('-') << std::setw(colWidth) << "+";
    fTxt << std::endl;
    /* |thr\str|   1 |   2 |   3 |   4 | */
    fTxt << "|thr\\str|";
    for ( int i = 0; i <= nMaxStride; ++i )
        fTxt << std::setfill(' ') << std::setw(7) << i;
    fTxt << std::endl;
    /* +-------+-----+-----+-----+-----+ */
    fTxt << "+-------+";
    for ( int i = 0; i <= nMaxStride; ++i )
        fTxt << std::setfill('-') << std::setw(colWidth) << "+";
    fTxt << std::endl;
    /* |    64 |     | 2.1 |     | 4.1 | */
    uint64_t fastestValue = 0;
    for ( int nThreads = 1; nThreads <= nMaxThreads; ++nThreads )
    {
        fTxt << std::setfill(' ') << std::setw(3) << nThreads;
        fDat << std::setfill(' ') << std::setw(3) << nThreads;
        for ( int nStride  = 0; nStride <= nMaxStride; ++nStride )
        {
            /* repeat measurement if the clock variable overflows ! */
            uint64_t minTiming = UINT64_MAX;
            for ( int i = 0; i < 25; ++i )
            {
                if ( testGlobalMemory )
                    kernelTestStridedGlobalMemory<<<1,nThreads>>>( nStride, (T_PREC*) dpReduced, dpTiming );
                else
                    kernelTestStridedSharedMemory<<<1,nThreads>>>( nStride, (T_PREC*) dpReduced, dpTiming );
                CUDA_ERROR( cudaDeviceSynchronize() );
                CUDA_ERROR( cudaMemcpy( &timing, dpTiming, sizeof(timing), cudaMemcpyDeviceToHost ) );
                /* don't use minimum timing, because it would
                 * be wrong for global memory access. In that case it is
                 * cached after the first access (tested). Although
                 * interestingly the cached access had the same shared memory
                 * latency, but no bank conflicts could be seen ...
                 * The reason for that is of course, because we have 32 banks
                 * a 4 bytes. Global memory access is always cached in
                 * cache lines of 128 bytes, meaning even if we need only 2
                 * floats, even if they would be in different "banks"
                 * judged by the global memory pointer, even then we would
                 * have to load two whole cache lines from shared memory */
                //if ( timing < minTiming )
                //    minTiming = timing;
                if ( timing != UINT64_MAX )
                {
                    minTiming = timing;
                    break;
                }
                else
                {
                    fTxt << "[End Cycle smaller then Start Cycle!!!]\n";
                }
            }

            fDat << std::setfill(' ') << std::setw(4) << minTiming;

            if ( fastestValue == 0 )
            {
                fastestValue = minTiming;
            }
            //else
            //    assert( fastestValue <= timing && "Please make the first benchmark in the loop the fastest" );
            if ( fastestValue != minTiming )
            {
                fTxt << std::setfill(' ') << std::setprecision(colWidth-3) << std::setw(colWidth-1)
                          << ( (float) minTiming / fastestValue ) << "|";
            }
            else
            {
                fTxt << std::setfill(' ') << std::setw(colWidth) << "|";
            }
        }
        fTxt << "\n";
        fDat << std::endl;
    }
    /* +-------+-----+-----+-----+-----+ */
    fTxt << "+-------+";
    for ( int i = 0; i <= nMaxStride; ++i )
        fTxt << std::setfill('-') << std::setw(colWidth) << "+";
    fTxt << std::endl;
    fTxt << "Empty columns mean 1 unit, other columns are multiples of the fastest access which is for no conflicts.\n";
    fTxt << "Fastest Access Unit = " << fastestValue << " cycles\n";
    fTxt << std::endl;

    CUDA_ERROR( cudaFree( dpReduced ) );
    CUDA_ERROR( cudaFree( dpTiming ) );
}


int main( void )
{
    cudaDeviceProp cudaProps;
    cudaGetDeviceProperties( &cudaProps, 0 );


    // for GTX 760 this is 12288
    /* we need quite a bit of memory for the stride benchmarks
     * Actually not needed, as the default value means it is automatically
     * chosen, but 48KiB ist still not enough:
     *    ptxas error   : Entry function '_Z10kernelTestIdEvjPT_Pm' uses too
     *                    much shared data (0x20000 bytes, 0xc000 max)
     * Note that 0xc000 = 49152 Byte */
    cudaDeviceSetCacheConfig( cudaFuncCachePreferShared );

    std::cout << "threads per SMX   : " << cudaProps.maxThreadsPerMultiProcessor << "\n";
    std::cout << "threads per block : " << cudaProps.maxThreadsPerBlock << "\n";

    std::cout << "\n\n";
    std::cout << "########################################\n";
    std::cout << "## Shared Memory Strided Access Tests ##\n";
    std::cout << "########################################\n";
    std::cout << "\n";

    /* 4 byte is default, therefore not needed normally, but it replaces the
     * output "default bank size" with "bank size: 4 bytes" */
    cudaDeviceSetSharedMemConfig( cudaSharedMemBankSizeFourByte );
    benchmarkStridedSharedMemoryAccess<char    >();
    benchmarkStridedSharedMemoryAccess<uint16_t>();
    benchmarkStridedSharedMemoryAccess<float   >();
    benchmarkStridedSharedMemoryAccess<double  >();
    cudaDeviceSetSharedMemConfig( cudaSharedMemBankSizeEightByte );
    benchmarkStridedSharedMemoryAccess<char    >();
    benchmarkStridedSharedMemoryAccess<uint16_t>();
    benchmarkStridedSharedMemoryAccess<float   >();
    benchmarkStridedSharedMemoryAccess<double  >();

    std::cout << "\n\n";
    std::cout << "########################################\n";
    std::cout << "## Global Memory Strided Access Tests ##\n";
    std::cout << "########################################\n";
    std::cout << "\n";

    benchmarkStridedSharedMemoryAccess<char    >( true );
    benchmarkStridedSharedMemoryAccess<uint16_t>( true );
    benchmarkStridedSharedMemoryAccess<float   >( true );
    benchmarkStridedSharedMemoryAccess<double  >( true );

    return 0;
}
