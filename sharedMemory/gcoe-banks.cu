/*
file=gcoe-banks; rm -f "$file"; nvcc -Xcompiler -Wall,-Wextra -O0 -arch=sm_30 -o "$file" -std=c++11 -I .. "../sharedMemory/$file.cu" && "./$file"
*/

#include <cassert>
#include <cstdint>              // uint64_t, uint16_t
#include <cstdlib>              // malloc, free
#include <iomanip>              // setw, setfill
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <cuda.h>
#include <cuda_runtime_api.h>   // cudaMalloc, cudaFree, cudaMemcpy

#include "cudacommon.hpp"


constexpr unsigned int nMaxThreads = 48;
constexpr unsigned int nMaxStride  = 128;
constexpr unsigned int nSMElements =  nMaxThreads * nMaxStride;
typedef uint64_t T_SMElement;

static __global__ void kernelSMTester
(
    uint64_t * const gpData         ,
    uint64_t   const nStrideElements,
    uint64_t   const nIterations    ,
    uint64_t * const result
)
{
    __shared__ T_SMElement smData[ nSMElements ];
    /* why is it inizialized ? */
    unsigned long long start = clock64();
    unsigned long long stop  = clock64();

    unsigned long sum      = 0;
    unsigned int  pos      = 0;
    unsigned int  posSum   = 0;
    unsigned int  clockSum = 0;

    // copy data
    for ( unsigned int i = threadIdx.x; i < nSMElements; i += blockDim.x )
        smData[i] =  gpData[i];
    __syncthreads();

    // warmup
    pos = threadIdx.x * nStrideElements;
    for ( unsigned int i = 0u; i < nIterations; ++i )
        pos = smData[pos];

    pos = threadIdx.x * nStrideElements;
    for ( unsigned int i = 0u; i < nIterations; ++i )
    {
        start   = clock64();
        pos     = smData[pos];
        // this is done to add a data dependency, for better clock measurments
        posSum += pos;
        stop    = clock64();
        sum    += stop - start;
    }

    /* calculate clock time */
    for ( unsigned int i = 0u; i < nIterations; ++i )
    {
        start     = clock64();
        stop      = clock64();
        clockSum += stop - start;
    }

    // write output here
    result[ 2*threadIdx.x + 0 ] = sum;
    result[ 2*threadIdx.x + 1 ] = posSum;
    result[ 2*threadIdx.x + 2 ] = clockSum;
}

int main()
{
    getCudaDeviceProperties();

    for ( unsigned int nThreads = 1; nThreads <= 32; ++nThreads )
    {
        std::stringstream fname;
        fname << "gcoe-timings-" << nThreads << "-threads.dat";
        std::ofstream fTimings( fname.str(), std::ios_base::out );

        unsigned int constexpr nIterations = 100;
        for ( unsigned int nStrideElements = 0; nStrideElements <= nMaxStride; ++nStrideElements )
        {
            MirroredVector< T_SMElement > data( nSMElements );
            for( unsigned int i = 0u; i < nSMElements; ++i )
                data.host[i] = ( i + nStrideElements ) % nSMElements;
            data.push();

            MirroredVector< uint64_t > results( 2*nMaxThreads );
            kernelSMTester<<< 1, 32 >>>( data.gpu, nStrideElements, nIterations, results.gpu );
            results.pop();

            double tAvg = (double) results.host[0] / nIterations;
            double tWoClock = (double) ( results.host[0] - results.host[2] ) / nIterations;
            std::cout
            << "Timing for stride " << std::setw(3) << nStrideElements
            << ": " << std::setw(8) << tAvg
            << " without clock: " << std::setw(8) << tWoClock
            << std::endl;

            fTimings<< nStrideElements << " " << tAvg << " " << tWoClock << "\n";
        }
    }
}
