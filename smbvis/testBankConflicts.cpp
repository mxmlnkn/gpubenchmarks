/*
file=testBankConflicts; rm -f "$file.exe"; nvcc -Xcompiler -Wall,-Wextra -O0 -arch=sm_30 "../sharedMemory/$file.cu" -o "$file.exe" -std=c++11 && "./$file.exe"
! Don't call with cuda-memcheck as it influences the time measurements !
*/

/**
@verbatim
      +----+----+----+----+----+----+ ... +----+----+----+----+----+
Bank: |  0 |  1 |  2 |  3 |  4 |  5 |     | 27 | 28 | 29 | 30 | 31 |
      +----+----+----+----+----+----+ ... +----+----+----+----+----+
Words:|  0 |  1 |  2 |  3 |  4 |  5 |     | 27 | 28 | 29 | 30 | 31 |
      | 32 | 33 | 34 | 35 | 36 | 37 |     | 59 | 60 | 61 | 62 | 63 |
        ...
1 word = 4 byte = 32 bit, but one bank has a capacity of 64 bits per cycle
        ^ use that scheme to visualize accesses!
           float * smData
conflict : x = smData[ threadIdx.x * 32 ]
broadcast: x = smData[ 0 ]
perfect  : x = smData[ threadIdx.x ]

With 64K -> 64*1024/4/32 = 512 rows

@see https://youtu.be/CZgM3DEBplE
@endverbatim
*/

#include <cassert>
#include <cstdint>              // uint64_t, uint16_t
#include <cstdlib>              // malloc, free
#include <iomanip>              // setw, setfill
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <cuda.h>
#include <cuda_runtime_api.h>   // cudaMalloc, cudaFree, cudaMemcpy

#include "ObjectCanvas/Canvas.hpp"
#include "sdlkram/colors/conversions.hpp" // turquese, ...
#include "cudacommon.hpp"


constexpr int nMaxThreads = 48;
constexpr int nMaxStride  = 128;


/*
Facts:
    http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory-3-0
     - Shared memory has 32 banks two addressing modes that are described below. [32 vs. 64 bit per bank]
     - The addressing mode can be queried using cudaDeviceGetSharedMemConfig() and set using cudaDeviceSetSharedMemConfig()
     - Each bank has a bandwidth of 64 bits per clock cycle.
     http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1g318e21528985458de8613d87da832b42
        __host__  __device__ cudaError_t cudaDeviceGetSharedMemConfig ( cudaSharedMemConfig * pConfig )
        The returned bank configurations can be either:
          * cudaSharedMemBankSizeFourByte
          * cudaSharedMemBankSizeEightByte

     - https://devtalk.nvidia.com/default/topic/834795/shared-memory-bank-size-mode-4-byte-vs-8-byte-kepler-/?offset=8
       -> no difference between 8-Byte and 4-Byte bank size, if __syncthreads() is used ... ... WHY?
*/

std::string printSharedMemoryConfig( void )
{
    std::stringstream out;
    out << "[Shared Memory] ";

    out << "Config: Prefer ";
    cudaFuncCache funcCache;
    cudaDeviceGetCacheConfig( &funcCache );
    switch ( funcCache )
    {
        case cudaFuncCachePreferNone  : out << "None"  ; break;
        case cudaFuncCachePreferShared: out << "Shared"; break;
        case cudaFuncCachePreferL1    : out << "L1"    ; break;
        case cudaFuncCachePreferEqual : out << "Equal" ; break;
        default: printf( "?" );
    }

    out << ", Bank Size: ";
    cudaSharedMemConfig config;
    cudaDeviceGetSharedMemConfig( &config );
    switch ( config )
    {
        case cudaSharedMemBankSizeDefault  : out << "Default"; break;
        case cudaSharedMemBankSizeFourByte : out << "4 Bytes"; break;
        case cudaSharedMemBankSizeEightByte: out << "8 Bytes"; break;
        default: printf( "?" );
    }
    out << "\n";
    return out.str();
}

#include "kernelTestStridedSharedMemory.cpp"

struct AccessPattern
{
    /* All can be written as something like threadIdx.x * stride + offset */
    int stride, offset, nThreads;
};

template<class T_PREC>
std::vector< uint64_t > benchmarkStridedSharedMemoryAccess
(
    AccessPattern const & pattern
)
{
    std::vector< uint64_t > vMinTimings( pattern.nThreads, UINT64_MAX );
    MirroredVector< uint64_t > vTimings( pattern.nThreads );
    MirroredVector< uint64_t > output(1);

    for ( int iRun = 0; iRun < 25; ++iRun )
    {
        kernelTestStridedSharedMemory< T_PREC ><<< 1, pattern.nThreads >>>(
            pattern.offset, pattern.stride, vTimings.gpu, output.gpu
        );
        vTimings.pop();
        for ( auto i = 0u; i < vMinTimings.size(); ++i )
            vMinTimings[i] = std::min( vMinTimings[i], vTimings.cpu[i] );
    }

    return vMinTimings;
}


int main( int argc, char ** argv )
{
    cudaDeviceProp cudaProps;
    cudaGetDeviceProperties( &cudaProps, 0 );

    AccessPattern pattern = { 6 /* stride */, 0 /* offset */, 256 };
    if ( argc >= 2 )
        pattern.stride = atoi( argv[1] );
    if ( argc >= 3 )
        pattern.nThreads = atoi( argv[2] );

    // for GTX 760 this is 12288
    /* we need quite a bit of memory for the stride benchmarks
     * Actually not needed, as the default value means it is automatically
     * chosen, but 48KiB ist still not enough:
     *    ptxas error   : Entry function '_Z10kernelTestIdEvjPT_Pm' uses too
     *                    much shared data (0x20000 bytes, 0xc000 max)
     * Note that 0xc000 = 49152 Byte */
    cudaDeviceSetCacheConfig( cudaFuncCachePreferShared );
    //cudaDeviceSetSharedMemConfig( cudaSharedMemBankSizeEightByte );

    std::cout << "threads per SMX   : " << cudaProps.maxThreadsPerMultiProcessor << "\n";
    std::cout << "threads per block : " << cudaProps.maxThreadsPerBlock << "\n";

    size_t nSharedMemoryBanks = 32;
    if ( cudaProps.major == 1 )
        nSharedMemoryBanks = 16;
    auto nWords = cudaProps.sharedMemPerBlock / ( 4 * nSharedMemoryBanks );
    nWords = std::min( (unsigned long) nMaxThreads, nWords );

    using namespace ObjectCanvas;
    Canvas canvas( 1200, 800 );
    auto const rectSize = 20;
    auto const linewidth = 2;   // should be even number, makes everything more logical
    auto smGrid = canvas.group(
        Props::x( 100 ) +
        Props::y( ( canvas.getHeight() - nSharedMemoryBanks * rectSize ) / 2 )
    );
    std::cout << "Shared memory has " << nSharedMemoryBanks << " banks and " << nWords << " rows totaling " << nSharedMemoryBanks * nWords * 4 << " B" << std::endl;
#if 0
    for ( auto iRow = 0u; iRow < nWords; ++iRow )
    for ( auto iCol = 0u; iCol < nSharedMemoryBanks; ++iCol )
    {
        smGrid->rectangle(
            Props::x     ( iCol * ( rectSize + 1 ) ) +
            Props::y     ( iRow * rectSize ) +
            Props::width ( rectSize ) +
            Props::height( rectSize ) +
            Props::strokewidth( 1 ) +
            Props::stroke( { 0,0,0,255 } )
        );
    }
#else
    auto const blackStroke = Props::stroke( { 0,0,0,255 } );
    auto const grayStroke  = Props::stroke( { 200,200,200,255 } );
    for ( auto iCol = 0u; iCol <= nWords; ++iCol )
    {
        smGrid->line(
            Props::x     ( iCol * rectSize ) +
            Props::y     ( 0 ) +
            Props::width ( 0 ) +
            Props::height( nSharedMemoryBanks * rectSize ) +
            Props::strokewidth( linewidth ) +
            ( /* iCol == 0 || */ iCol == nWords ? blackStroke : grayStroke )
        );
    }
    for ( auto iRow = 0u; iRow <= nSharedMemoryBanks; ++iRow )
    {
        smGrid->line(
            Props::x     ( 0 ) +
            Props::y     ( iRow * rectSize ) +
            Props::width ( nWords * rectSize ) +
            Props::height( 0 ) +
            Props::strokewidth( linewidth ) +
            Props::stroke( { 0,0,0,255 } )
        );
    }
#endif

    /* draw access pattern */
    std::cout << "Access smData[ " << pattern.offset << " + "
        << pattern.stride << " * threadIdx.x ]\n";
    auto const highlighted = smGrid->group();
    Color lila{ 0,0,0,0.8 };
    colors::hslToRgb( colors::hues::violet, 0.8, 0.5, &lila.r, &lila.g, &lila.b );
    lila *= 255;
    std::cout << "lila = " << lila << std::endl;

    for ( auto iThread = 0u; iThread < pattern.nThreads; ++iThread )
    {
        auto const iAccess = pattern.offset + iThread * pattern.stride;
        auto const iBank   = iAccess % nSharedMemoryBanks;
        auto const iRow    = iAccess / nSharedMemoryBanks;
        std::cout << "Thread " << std::setw(2) << iThread << "/"
            << std::setw(2) << pattern.nThreads << " -> i=" << iAccess << ", "
            << "Bank " << std::setw(2) << iBank << ", "
            << "Row"   << std::setw(2) << iRow << std::endl;
        highlighted->rectangle(
            Props::x( iRow  * rectSize + linewidth/2 ) +
            Props::y( iBank * rectSize + linewidth/2 ) +
            Props::width ( rectSize - linewidth ) +
            Props::height( rectSize - linewidth ) +
            Props::fill( lila ) // { 255,0,0,255 }
        );
    }

    std::cout << printSharedMemoryConfig();
    auto const vTimings = benchmarkStridedSharedMemoryAccess< float >( pattern );
    std::cout << "Timings per thread: ";
    for ( auto const & x : vTimings )
        std::cout << x << " ";
    std::cout << std::endl;

    cudaDeviceReset();
   // canvas.show();
    return 0;
}
