/*
nvcc -DKERNEL_STANDALONE -x cu -ptx -std=c++11 ../smbvis/kernelTestStridedSharedMemory.cpp
*/

#ifdef KERNEL_STANDALONE
#include <cstdint>              // uint64_t, uint16_t

constexpr int nMaxThreads = 48;
constexpr int nMaxStride  = 128;
#endif

#if 0
template<class T_PREC>
__global__ void kernelTestStridedSharedMemory
(
    unsigned   const rnOffset  ,
    unsigned   const rnStride  ,
    uint64_t * const rdpTimings,
    uint64_t * const rdpOutput
)
{
    /* shared memory has 32 banks a 4 byte normally */
    constexpr unsigned nElements = nMaxStride * nMaxThreads;
    __volatile__ __shared__ T_PREC smData[ nElements ]; // else compiler will load one time into register and then reuse that value .3

    /* we don't want to measure pointer arithmetic, so save pointer and init shared array */
    auto elem = &smData[ rnOffset + threadIdx.x * rnStride ];
    //for ( unsigned i = threadIdx.x; i < nElements; i += blockDim.x )
    //    smData[i] = T_PREC(1);
    //__syncthreads();

    /* measure measurement offset, meaning the call to clock64 to subtract it */
    //__syncthreads();
    auto t00 = clock();
    //__syncthreads();
    auto t10 = clock();
    auto dtOffset = t10 - t00;
    // http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#time-function
    // clock64: with -G this is 221 on GTX 760, without it's 105, -O0 doesn't influence this
    // clock  : with -G this is 137 on GTX 760, without it's 64 , -O0 doesn't influence this

    /* clock64 actually returns the GPU clocks, meaning we won't need a for
     * loop to wait and wait until the timer actually has enough precision to
     * measure that result. Here we can measure every cycle */
    //__syncthreads();
    T_PREC x = 1;
    auto t0 = clock();
    #define NLOOPS 256
    #pragma unroll  // in order to somehow get out the pipelining of this micro benchmark
    for ( int i = 0; i < NLOOPS; ++i )
        x *= *elem;  // doesn't get optimized away, even without -G and without writing to some kind of output :S
    //__syncthreads(); // this seems to an effect on bank conflicts... ,see top
    auto t1 = clock();

    rdpTimings[ threadIdx.x ] = t1-t0; //t1 > t0+dtOffset ? t1-t0-dtOffset : UINT64_MAX;
    *rdpOutput += x;
}
#endif

/* this version tries to max out throughput, because I think that is the reason why nothing degrades when using more threads... the bandwidth is enough that more threads leading to bank conflicts don't actually lead to any degradation! */
template<class T_PREC>
__global__ void kernelTestStridedSharedMemory
(
    unsigned   const rnOffset  ,
    unsigned   const rnStride  ,
    uint64_t * const rdpTimings,
    uint64_t * const rdpOutput
)
{
    /* shared memory has 32 banks a 4 byte normally */
    constexpr unsigned nElements = nMaxStride * nMaxThreads;
    __volatile__ __shared__ T_PREC smData[ nElements ];
    __volatile__ __shared__ T_PREC smTmp [ nElements ];

    auto elem = &smData[ rnOffset + threadIdx.x * rnStride ];

    #define NLOOPS 64   // because this is the latency for *

    auto t0 = clock();
    T_PREC x = 1;
    #pragma unroll  // in order to somehow get out the pipelining of this micro benchmark
    for ( int i = 0; i < NLOOPS; ++i )
        x *= *elem;  // doesn't get optimized away, even without -G and without writing to some kind of output :S
    //__syncthreads(); // this seems to an effect on bank conflicts... ,see top
    auto t1 = clock();

    rdpTimings[ threadIdx.x ] = t1-t0; //t1 > t0+dtOffset ? t1-t0-dtOffset : UINT64_MAX;
    *rdpOutput += x;
}

template
__global__ void kernelTestStridedSharedMemory<float>
(
    unsigned   const rnOffset  ,
    unsigned   const rnStride  ,
    uint64_t * const rdpTimings,
    uint64_t * const rdpOutput
);
